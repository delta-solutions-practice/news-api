package ru.deltasolutions.newsapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.collect.Lists;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import ru.deltasolutions.newsapi.constants.ErrorCodes;
import ru.deltasolutions.newsapi.constants.Statuses;
import ru.deltasolutions.newsapi.models.*;
import ru.deltasolutions.newsapi.models.Error;

import java.io.IOException;
import java.io.InputStreamReader;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class NewsApiClient {

    private String BASE_URL = "https://newsapi.org/v2/";

    private HttpClient httpClient;

    private String apiKey;

    public NewsApiClient(String apiKey) {
        this.apiKey = apiKey;

        List<Header> headers = Lists.newArrayList(new BasicHeader("user-agent", "News-API-java/0.1"),
                                                    new BasicHeader("x-api-key", this.apiKey));
        httpClient = HttpClients.custom().setDefaultHeaders(headers).build();
    }

    public ArticlesResult getTopHeadlines(TopHeadlinesRequest request) {
        List<String> queryParams = setupArticlesRequestParams(request);

        if (request.getCategory() != null) {
            queryParams.add("category=" + request.getCategory().toString());
        }

        if (request.getCountry() != null) {
            queryParams.add("country=" + request.getCountry().toString());
        }

        String queryString = String.join("&", queryParams);

        return makeRequest("top-headlines", queryString);
    }

    public ArticlesResult getEverything(EverythingRequest request) {
        List<String> queryParams = setupArticlesRequestParams(request);

        if ( (request.getDomains() != null) && !(request.getDomains().isEmpty()) ) {
            queryParams.add("domains=" + String.join(",", request.getDomains()));
        }

        if (request.getFrom() != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;
            queryParams.add("from=" + formatter.format(request.getFrom()));
        }

        if (request.getTo() != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;
            queryParams.add("to=" + formatter.format(request.getTo()));
        }

        if (request.getSortBy() != null) {
            queryParams.add("sortBy=" + request.getSortBy().toString());
        }

        String queryString = String.join("&", queryParams);

        return makeRequest("everything", queryString);
    }

    private List<String> setupArticlesRequestParams(ArticlesRequest request) {
        ArrayList<String> queryParams = new ArrayList<>();

        if ( (request.getQ() != null) && !( request.getQ().isEmpty() ) ) {
            queryParams.add("q=" + request.getQ());
        }

        if ( (request.getSources() != null) && !(request.getSources().isEmpty()) ) {
            queryParams.add("sources=" + String.join(",", request.getSources()));
        }

        if (request.getLanguage() != null) {
            queryParams.add("language=" + request.getLanguage().toString());
        }

        if (request.getPage() > 1) {
            queryParams.add("page=" + request.getPage());
        }

        if (request.getPageSize() > 0) {
            queryParams.add("pageSize=" + request.getPageSize());
        }

        return queryParams;
    }

    private ArticlesResult makeRequest(String endpoint, String queryString) {
        ArticlesResult articlesResult = new ArticlesResult();

        HttpGet httpGet = new HttpGet(BASE_URL + endpoint + "?" + queryString);
        try (CloseableHttpResponse response = (CloseableHttpResponse) httpClient.execute(httpGet)) {

            HttpEntity entity = response.getEntity();
            if (entity.getContentLength() > 0) {
                ObjectMapper objectMapper = new ObjectMapper()
                        .registerModule(new JavaTimeModule());
                ApiResponse apiResponse = objectMapper.readValue(new InputStreamReader(entity.getContent()),
                        ApiResponse.class);
                articlesResult.setStatus(apiResponse.getStatus());
                if (articlesResult.getStatus() == Statuses.ok) {
                    articlesResult.setTotalResults(apiResponse.getTotalResults());
                    articlesResult.setArticles(apiResponse.getArticles());
                }
                else {
                    Error error = new Error();
                    error.setCode(apiResponse.getCode());
                    error.setMessage(apiResponse.getMessage());
                }
            }
            else {
                articlesResult.setStatus(Statuses.error);
                Error error = new Error();
                error.setCode(ErrorCodes.unexpectedError);
                error.setMessage("The API returned an empty response. Are you connected to the internet?");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return articlesResult;
    }

}
