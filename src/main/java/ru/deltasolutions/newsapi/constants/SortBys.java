package ru.deltasolutions.newsapi.constants;

public enum SortBys {
    popularity,
    publishedAt,
    relevancy
}