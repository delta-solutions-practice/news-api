package ru.deltasolutions.newsapi.constants;

public enum Languages {
    af,
    an,
    ar,
    az,
    bg,
    bn,
    br,
    bs,
    ca,
    cs,
    cy,
    da,
    de,
    el,
    en,
    eo,
    es,
    et,
    eu,
    fa,
    fi,
    fr,
    gl,
    he,
    hi,
    hr,
    ht,
    hu,
    hy,
    id,
    is,
    it,
    jp,
    jv,
    kk,
    ko,
    la,
    lb,
    lt,
    lv,
    mg,
    mk,
    ml,
    mr,
    ms,
    nl,
    nn,
    no,
    oc,
    pl,
    pt,
    ro,
    ru,
    sh,
    sk,
    sl,
    sq,
    sr,
    sv,
    sw,
    ta,
    te,
    th,
    tl,
    tr,
    uk,
    ur,
    vi,
    vo,
    zh
}