package ru.deltasolutions.newsapi.constants;

public enum Categories {
    business,
    entertainment,
    health,
    science,
    sports,
    technology
}