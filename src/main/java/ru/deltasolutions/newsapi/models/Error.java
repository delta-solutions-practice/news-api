package ru.deltasolutions.newsapi.models;

import ru.deltasolutions.newsapi.constants.ErrorCodes;

public class Error {

    private ErrorCodes code;
    private String message;

    public ErrorCodes getCode() {
        return code;
    }

    public void setCode(ErrorCodes code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
