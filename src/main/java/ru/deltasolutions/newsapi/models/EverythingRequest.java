package ru.deltasolutions.newsapi.models;

import ru.deltasolutions.newsapi.constants.Languages;
import ru.deltasolutions.newsapi.constants.SortBys;

import java.time.Instant;
import java.util.List;

public class EverythingRequest extends ArticlesRequest {

    private List<String> domains;
    private Instant from;
    private Instant to;
    private SortBys sortBy;

    public List<String> getDomains() {
        return domains;
    }

    public Instant getFrom() {
        return from;
    }

    public Instant getTo() {
        return to;
    }

    public SortBys getSortBy() {
        return sortBy;
    }

    public void setDomains(List<String> domains) {
        this.domains = domains;
    }

    public void setFrom(Instant from) {
        this.from = from;
    }

    public void setTo(Instant to) {
        this.to = to;
    }

    public void setSortBy(SortBys sortBy) {
        this.sortBy = sortBy;
    }

    public static class Builder {
        private EverythingRequest request;

        public Builder() {
            request = new EverythingRequest();
        }

        public Builder withQ(String q) {
            request.q = q;
            return this;
        }

        public Builder withSources(List<String> sources) {
            request.sources = sources;
            return this;
        }

        public Builder withDomains(List<String> domains) {
            request.domains = domains;
            return this;
        }

        public Builder withFrom(Instant from) {
            request.from = from;
            return this;
        }

        public Builder withTo(Instant to) {
            request.to = to;
            return this;
        }

        public Builder withLanguage(Languages language) {
            request.language = language;
            return this;
        }

        public Builder withSortBy(SortBys sortBy) {
            request.sortBy = sortBy;
            return this;
        }

        public Builder withPage(int page) {
            request.page = page;
            return this;
        }

        public Builder withPageSize(int pageSize) {
            request.pageSize = pageSize;
            return this;
        }

        public EverythingRequest build() {
            return request;
        }
    }

}
