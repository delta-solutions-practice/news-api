package ru.deltasolutions.newsapi.models;

import ru.deltasolutions.newsapi.constants.Statuses;

import java.util.List;

public class ArticlesResult {

    private Statuses status;
    private Error error;
    private int TotalResults;
    private List<Article> articles;

    public Statuses getStatus() {
        return status;
    }

    public void setStatus(Statuses status) {
        this.status = status;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public int getTotalResults() {
        return TotalResults;
    }

    public void setTotalResults(int totalResults) {
        TotalResults = totalResults;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }
}
