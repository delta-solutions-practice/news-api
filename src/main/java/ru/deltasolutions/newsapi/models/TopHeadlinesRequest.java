package ru.deltasolutions.newsapi.models;

import ru.deltasolutions.newsapi.constants.Categories;
import ru.deltasolutions.newsapi.constants.Countries;
import ru.deltasolutions.newsapi.constants.Languages;

import java.util.List;

public class TopHeadlinesRequest extends ArticlesRequest {

    private Categories category;
    private Countries country;

    public Categories getCategory() {
        return category;
    }

    public Countries getCountry() {
        return country;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }

    public void setCountry(Countries country) {
        this.country = country;
    }

    public static class Builder {
        private TopHeadlinesRequest request;

        public Builder() {
            request = new TopHeadlinesRequest();
        }

        public Builder withQ(String q) {
            request.q = q;
            return this;
        }

        public Builder withSources(List<String> sources) {
            request.sources = sources;
            return this;
        }

        public Builder withCategory(Categories category) {
            request.category = category;
            return this;
        }

        public Builder withLanguage(Languages language) {
            request.language = language;
            return this;
        }

        public Builder withCountry(Countries country) {
            request.country = country;
            return this;
        }

        public Builder withPage(int page) {
            request.page = page;
            return this;
        }

        public Builder withPageSize(int pageSize) {
            request.pageSize = pageSize;
            return this;
        }

        public TopHeadlinesRequest build() {
            return request;
        }
    }

}
