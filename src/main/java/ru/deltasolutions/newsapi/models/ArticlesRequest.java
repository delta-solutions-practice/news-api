package ru.deltasolutions.newsapi.models;

import ru.deltasolutions.newsapi.constants.Languages;

import java.util.List;

public abstract class ArticlesRequest {

    String q;
    List<String> sources;
    Languages language;
    int page;
    int pageSize;

    public String getQ() {
        return q;
    }

    public List<String> getSources() {
        return sources;
    }

    public Languages getLanguage() {
        return language;
    }

    public int getPage() {
        return page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public void setSources(List<String> sources) {
        this.sources = sources;
    }

    public void setLanguage(Languages language) {
        this.language = language;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
