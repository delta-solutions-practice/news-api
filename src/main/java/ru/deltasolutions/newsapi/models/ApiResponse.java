package ru.deltasolutions.newsapi.models;

import ru.deltasolutions.newsapi.constants.ErrorCodes;
import ru.deltasolutions.newsapi.constants.Statuses;

import java.util.List;

public class ApiResponse {

    private Statuses status;
    private ErrorCodes code;
    private String message;
    private List<Article> Articles;
    private int totalResults;

    public Statuses getStatus() {
        return status;
    }

    public void setStatus(Statuses status) {
        this.status = status;
    }

    public ErrorCodes getCode() {
        return code;
    }

    public void setCode(ErrorCodes code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Article> getArticles() {
        return Articles;
    }

    public void setArticles(List<Article> articles) {
        Articles = articles;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

}
